﻿using System;
using System.Net;
using System.Threading;

namespace UdpSample
{
    class Program
    {
        private static IPAddress remoteIPAddress;
        private static int remotePort;

        static void Main(string[] args)
        {
            try
            {
                // Получаем данные, необходимые для соединения
                Console.WriteLine("Укажите локальный порт");
                var localPort = Convert.ToInt32(Console.ReadLine());
                var udpChat = new UdpChat(localPort);

                Console.WriteLine("Укажите удаленный порт");
                remotePort = Convert.ToInt16(Console.ReadLine());

                Console.WriteLine("Укажите удаленный IP-адрес");
                remoteIPAddress = IPAddress.Parse(Console.ReadLine());

                // Создаем поток для прослушивания
                Thread tRec = new Thread(new ThreadStart(udpChat.Receiver));
                tRec.Start();

                while (true)
                {
                    udpChat.Send(Console.ReadLine(), remoteIPAddress.Address, remotePort: remotePort);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Возникло исключение: " + ex.ToString() + "\n  " + ex.Message);
            }

        }
    }
}