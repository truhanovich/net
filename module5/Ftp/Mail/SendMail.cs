﻿using System.Net;
using System.Net.Mail;

namespace Mail
{
    internal class SendMail : ISendMail
    {
        public void Send(MailModel mailModel)
        {
            using (MailMessage mail = new MailMessage())
            {
                mail.From = new MailAddress(mailModel.From);
                mail.To.Add(new MailAddress(mailModel.To));
                mail.Subject = mailModel.Caption;
                mail.Body = mailModel.Message;

                if (!string.IsNullOrEmpty(mailModel.AttachFile))
                    mail.Attachments.Add(new Attachment(mailModel.AttachFile));

                SmtpClient client = new SmtpClient
                {
                    Host = mailModel.SmptServer,
                    Port = 587,
                    Credentials = new NetworkCredential(mailModel.From, mailModel.Password),
                    EnableSsl = true
                };
                
                client.Send(mail);
            }
        }
    }
}
