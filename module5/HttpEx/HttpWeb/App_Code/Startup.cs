﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(HttpWeb.Startup))]
namespace HttpWeb
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
