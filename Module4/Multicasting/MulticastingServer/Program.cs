﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace MulticastingServer
{
    static class Program
    {
        static void Main()
        {
            UdpClient udpClient = new UdpClient();

            var multicastAddress = IPAddress.Parse("239.0.0.222");
            udpClient.JoinMulticastGroup(multicastAddress);
            var remoteIp = new IPEndPoint(multicastAddress, 2222);

            Console.WriteLine("Press ENTER to start sending messages");
            Console.ReadLine();

            for (int i = 0; i <= 8000; i++)
            {
                var  buffer = Encoding.Unicode.GetBytes(i.ToString());
                udpClient.Send(buffer, buffer.Length, remoteIp);
                Console.WriteLine("Sent " + i);
            }

            Console.WriteLine("All Done! Press ENTER to quit.");
            Console.ReadLine();
        }
    }
}
