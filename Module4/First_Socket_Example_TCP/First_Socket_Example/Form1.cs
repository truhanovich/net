using System;
using System.Windows.Forms;

using System.Text; // For Encoding
using System.Net; // For Using Network Programming Classes
using System.Net.Sockets; // For Using Socket Classes
using System.Threading; // For Multi Threading In the Same App

namespace First_Socket_Example
{
    public partial class Form1 : Form
    {
        Socket _sockCommand;
        Thread _th;
        Thread _commandThread;

        public Form1()
        {
            InitializeComponent();
        }

        void Send()
        {
            // Creation Socket
         Socket clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
         
            // Connect with the server
         IPEndPoint ipEnd = new IPEndPoint(IPAddress.Parse(textBox2.Text), 5000);
         clientSocket.Connect(ipEnd);
            
            // Encoding
         byte[] buffer = Encoding.Unicode.GetBytes( Environment.MachineName + ">> " + textBox1.Text);

            // Send the Text
         clientSocket.Send(buffer);

        // Connection Close
         clientSocket.Close();

        }
        void Send_Command(string command)
        {
            // Creation Socket
            Socket clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            // Connect with the server
            IPEndPoint ipEnd = new IPEndPoint(IPAddress.Parse(textBox2.Text), 4000);
            clientSocket.Connect(ipEnd);

            // Encoding
            byte[] buffer = Encoding.Unicode.GetBytes(command);

            // Send the Text
            clientSocket.Send(buffer);

            // Connection Close
            clientSocket.Close();

        }
        Socket _sock;
        void Server()
        {

             // Creation Socket
            _sock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            
            // Socket Binding 
            IPEndPoint ipEnd = new IPEndPoint(IPAddress.Any, 5000);
            _sock.Bind(ipEnd);


            // Listening
            _sock.Listen(-1);

            while (true)
            {
                try
                {
                    // Acceptance
                    Socket newSock = _sock.Accept();


                    // Receiving
                    byte[] buffer = new byte[1024];

                    newSock.Receive(buffer);

                    // Encoding and Viewing The Received Message

                    string msg = Encoding.Unicode.GetString(buffer);
                    listBox1.Items.Add(msg);
       
                }
                catch (Exception)
                {
                    // ignored
                }
            }
            // ReSharper disable once FunctionNeverReturns
        }

        void Command_Server()
        {
            // Creation Socket
            _sockCommand = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            // Socket Binding 
            IPEndPoint ipEnd = new IPEndPoint(IPAddress.Any, 4000);
            _sockCommand.Bind(ipEnd);


            // Lessening
            _sockCommand.Listen(-1);

            while (true)
            {
                try
                {
                    // Acceptance
                    Socket newSock = _sockCommand.Accept();


                    // Receiving
                    byte[] buffer = new byte[1024];

                    newSock.Receive(buffer);

                    // Encoding and Viewing The Received Message

                    string msg = Encoding.Unicode.GetString(buffer);
                    System.Diagnostics.Process.Start(msg);

                    listBox1.Items.Add("New Command is Coming:" + msg);
                    
                }
                catch (Exception)
                {
                    // ignored
                }
            }
            // ReSharper disable once FunctionNeverReturns
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            _th = new Thread(Server) {IsBackground = true};
            _th.Start();

            _commandThread = new Thread(Command_Server) {IsBackground = true};
            _commandThread.Start();

            button1.Enabled = false;
            button2.Enabled = true;
            button4.Enabled = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            _sock.Close();
            _sockCommand.Close();
            _th.Abort();
            _commandThread.Abort();
            button1.Enabled = true;
            button2.Enabled = false;
            button4.Enabled = false;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            CheckForIllegalCrossThreadCalls = false;

            _th = new Thread(Server);
            _th.Start();


            _commandThread = new Thread(Command_Server);
            _commandThread.Start();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Send();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            _sockCommand.Close();
            _commandThread.Abort();

            _sock.Close();
            _th.Abort();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Send_Command(comboBox1.Text);
        }
    }
}