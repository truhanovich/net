﻿using System;
using System.Threading;

namespace Broadcast
{
    static class Program
    {
        static void Main()
        {
            var udp = new UdpSendersReceives();
            udp.Start();
            Console.WriteLine("Enter 's' fro send;");
            Console.WriteLine("Enter 'x' fro stop;");

            do
            {
                if (Console.KeyAvailable)
                {
                    var cki = Console.ReadKey(true);
                    switch (cki.KeyChar)
                    {
                        case 's':
                            udp.Send(new Random().Next().ToString());
                            break;
                        case 'x':
                            udp.Stop();
                            return;
                    }
                }
                Thread.Sleep(10);
            } while (true);
        }
    }
}
