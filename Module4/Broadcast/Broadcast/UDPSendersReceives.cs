﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace Broadcast
{
    public class UdpSendersReceives
    {
        const int PortNumber = 15000;

        readonly Thread _thread = null;
        private readonly UdpClient _udp = new UdpClient(PortNumber);

        public void Start()
        {
            if (_thread != null)
            {
                throw new Exception("Already started, stop first");
            }
            Console.WriteLine("Started listening");
            StartListening();
        }
        public void Stop()
        {
            try
            {
                _udp.Close();
                Console.WriteLine("Stopped listening");
            }
            catch { /* don't care */ }
        }

        private void StartListening()
        {
            _udp.BeginReceive(Receive, new object());
        }

        private void Receive(IAsyncResult ar)
        {
            var ip = new IPEndPoint(IPAddress.Any, PortNumber);
            var bytes = _udp.EndReceive(ar, ref ip);
            var message = Encoding.ASCII.GetString(bytes);
            Console.WriteLine("From {0} received: {1} ", ip.Address, message);
            StartListening();
        }

        public void Send(string message)
        {
            var client = new UdpClient();
            var ip = new IPEndPoint(IPAddress.Parse("255.255.255.255"), PortNumber);
            var bytes = Encoding.ASCII.GetBytes(message);
            client.Send(bytes, bytes.Length, ip);
            client.Close();
            Console.WriteLine("Sent: {0} ", message);
        }
    }
}
